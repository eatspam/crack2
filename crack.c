#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

int file_length(char *filename)
{
    struct stat fileinfo;
    if (stat(filename, &fileinfo) == -1)
        return -1;
    else
        return fileinfo.st_size;
}

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *ghash = md5(guess, PASS_LEN);
    printf("%s\n", hash);
    
    // Compare the two hashes
    // Free any malloc'd memory
    // free(hash);
    if (strcmp (hash, ghash))
        return 1;
        
    return 0;
}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{
    // obtain length of file
    int len = file_length(filename);
    if (len == -1)
    {
        printf("Couldn't get length of file %s\n", filename);
        exit(1);
    }
    
    // allocate memory for the entire file
    char *file_contents = malloc(len);
    
    // read entire file into file_contents
    FILE *fp = fopen(filename, "r");
    if (!fp)
    {
        printf("Couldn't open %s for reading\n", filename);
        exit(1);
    }
    fread(file_contents, 1, len, fp);
    fclose(fp);
    
    // replace \n with \0
    // also keep count as we go
    int line_count = 0;
    for (int i = 0; i < len; i++)
    {
        if (file_contents[i] == '\n')
        {
            file_contents[i] = '\0';
            line_count++;
            size = &line_count;
        }
    }
    
    // allocate array of pointers to each line, with additional space for a NULL at the end
    char **lines = malloc((line_count+1) * sizeof(char));
    
    // fill in each entry with address of corresponding line
    int c = 0;
    for (int i = 0; i < line_count; i++)
    {
        lines[i] = &file_contents[c];
        
        // scan forward to find next line
        while (file_contents[c] != '\0') c++;
        c++;
    }
    
    // store null at end of the array
    lines[line_count] = NULL;
    
    // return the address of the data structure
    return lines;
    

}

char **readfile(char *filename)
{
    // obtain length of file
    int len = file_length(filename);
    if (len == -1)
    {
        printf("Couldn't get length of file %s\n", filename);
        exit(1);
    }
    
    // allocate memore for the entire fie
    char *file_contents = malloc(len);
    
    // read entire file into file_contents
    FILE *fp = fopen(filename, "r");
    if (!fp)
    {
        printf("Couldn't open %s for reading\n", filename);
        exit(1);
    }
    fread(file_contents, 1, len, fp);
    fclose(fp);
    
    // replace \n with \0
    // also keep count as we go
    int line_count = 0;
    for (int i = 0; i < len; i++)
    {
        if (file_contents[i] == '\n')
        {
            file_contents[i] = '\0';
            line_count++;
        }
    }
    
    // allocate array of pointers to each line, with additional space for a NULL at the end
    char **lines = malloc((line_count+1) * sizeof(char));
    
    // fill in each entry with address of corresponding line
    int c = 0;
    for (int i = 0; i < line_count; i++)
    {
        lines[i] = &file_contents[c];
        
        // scan forward to find next line
        while (file_contents[c] != '\0') c++;
        c++;
    }
    
    // store null at end of the array
    lines[line_count] = NULL;
    
    // return the address of the data structure
    return lines;
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of strings.
    int dlen;
    char **dict = read_dictionary(argv[2], &dlen);
    
    // Open the hash file for reading.
    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    
   char **hash = readfile(argv[1]);   
    
    int i = 0;
    while (hash[i] != NULL)
    {
        printf("%s\n", hash[i]);
        
    }
    
}
